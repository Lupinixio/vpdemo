package com.vp.movie.abstraction.dto

interface Movie {
    val title: String
    val year: String
    val imdbID: String
    val poster: String
}